package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import dao.LocationDao;
import model.Country;
import model.Location;

@CrossOrigin("*")
@RestController
public class LocationController {
	
	@Autowired
	LocationDao dao;
	
	@GetMapping(value="/locations", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Location> showLocationList(){
		return dao.getLocations();
	}
	
	@GetMapping(value="/locations/byCity/{city}", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Location> showLocationFromCity(@PathVariable("city") String city) {
		return dao.getLocationsByCity(city);
	}
	
	@GetMapping(value="/locations/byId/{id}", produces=MediaType.APPLICATION_JSON_VALUE)
	public Location showLocationWithId(@PathVariable("id") int id) {
		return dao.getLocationById(id);
	}
	
	@GetMapping(value="/locations/countries", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Country> showCountriesList(){
		return dao.getCountries();
	}
	
	@GetMapping(value="/locations/byCountry/{countryId}", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Location> showLocationFromCountry(@PathVariable("countryId") String countryId){
		return dao.getLocationsByCountry(countryId);
	}
	
	@PostMapping(value="/locations/", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addLocation(@RequestBody Location location) {
		dao.addLocation(location);
	}
	
	@PutMapping(value="/locations/", consumes=MediaType.APPLICATION_JSON_VALUE)
	public void modifyLocation(@RequestBody Location location) {
		dao.modifyLocation(location);
	}
	
	@DeleteMapping(value="/locations/byId/{id}")
	public void deleteLocationById(@PathVariable("id") int id) {
		dao.deleteLocationById(id);
	}
	
	@DeleteMapping(value="/locations/byAddress/{address}")
	public void deleteLocationByAddress(@PathVariable("address") String address) {
		dao.deleteLocationByAddress(address);
	}
}
