package dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mapper.LocationMapper;
import model.Country;
import model.Location;

@Repository("LocationService")
public class LocationDaoImpl implements LocationDao {

	@Autowired
	LocationMapper mapper;
	
	@Override
	public List<Location> getLocations() {
		return mapper.getLocations();
	}

	@Override
	public List<Location> getLocationsByCity(String city) {
		return mapper.getLocationsByCity(city);
	}

	@Override
	public Location getLocationById(int id) {
		return mapper.getLocationById(id);
	}

	@Override
	public List<Country> getCountries() {
		return mapper.getCountries();
	}

	@Override
	public List<Location> getLocationsByCountry(String countryId) {
		return mapper.getLocationsByCountry(countryId);
	}

	@Override
	public void addLocation(Location location) {
		mapper.addLocation(location);
	}

	@Override
	public void modifyLocation(Location location) {
		mapper.modifyLocation(location);
	}

	@Override
	public void deleteLocationById(int id) {
		mapper.deleteLocationById(id);
		
	}

	@Override
	public void deleteLocationByAddress(String address) {
		mapper.deleteLocationByAddress(address);
	}
}
