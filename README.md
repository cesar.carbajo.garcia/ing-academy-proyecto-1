# ING-ACADEMY Proyecto 1

Este proyecto consta de:
- Un microservicio SpringBoot que conecta con una base de datos autogenerada de Oracle que puede realizar las siguientes operaciones:
  + Obtener una lista de direcciones de la tabla "locations". Puede obtener también listas según el país y la ciudad. (GET)
  + Obtener una dirección a partir del id. (GET)
  + Obtener una lista de paises de la tabla "countries". (GET)
  + Añadir una dirección a la tabla a partir de un JSON del body (POST)
  + Actualizar una dirección de la tabla a partir de un JSON del body (PUT)
  + Eliminar una dirección a partir del id o del nombre obtenido de la url (DELETE)
- Un cliente html5 con JavaScript que utiliza angular y jQuery con las siguientes vistas:
  + Un índice que permite acceder a las demás vistas.
  + Una tabla que lista todas las direcciones y que permite eliminar una concreta.
  + Un formulario que permite añadir una dirección o actualizar una existente con un desplegable que te deja escoger un país de los existentes.
  + Una entrada de texto que te permite eliminar una dirección a partir de su nombre.