package mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import model.Country;
import model.Location;

public interface LocationMapper {
	
	@Select("SELECT * FROM locations")
	List<Location> getLocations();
	
	@Select("SELECT * FROM locations WHERE city = #{city}")
	List<Location> getLocationsByCity(String city);
	
	@Select("SELECT * FROM locations WHERE location_id = #{id}")
	Location getLocationById(int id);
	
	@Select("SELECT * FROM countries")
	List<Country> getCountries();
	
	@Select("SELECT * FROM locations WHERE country_id = #{countryId}")
	List<Location> getLocationsByCountry(String countryId);
	
	@Insert("INSERT INTO locations (street_address, postal_code, city, state_province, country_id)"
			+ "VALUES(#{street_address}, #{postal_code}, #{city}, #{state_province}, #{country_id})")
	@Options(useGeneratedKeys=true, keyProperty="location_id", keyColumn = "location_id")
	void addLocation(Location location);
	
	@Update("UPDATE locations SET postal_code = #{postal_code}, city = #{city}, "
								+ "state_province = #{state_province}, country_id = #{country_id} "
								+ "WHERE street_address = #{street_address}")
	void modifyLocation(Location location);
	
	@Delete("DELETE FROM locations WHERE location_id = #{id}")
	void deleteLocationById(int id);
	
	@Delete("DELETE FROM locations WHERE street_address = #{address}")
	void deleteLocationByAddress(String address);
}
