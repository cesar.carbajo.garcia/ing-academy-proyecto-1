package dao;

import java.util.List;

import model.Country;
import model.Location;

public interface LocationDao {
	
	List<Location> getLocations();
	
	List<Location> getLocationsByCity(String city);
	
	Location getLocationById(int id);
	
	List<Country> getCountries();
	
	List<Location> getLocationsByCountry(String countryId);
	
	void addLocation(Location location);
	
	void modifyLocation(Location location);
	
	void deleteLocationById(int id);
	
	void deleteLocationByAddress(String address);
}
